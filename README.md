Cleaning room for extraneous unvanquished assets
------------------------------------------------

This temporary repository is a place to sort files from these archives:

- `unvanquished_0.25.0.pk3`
- `unvanquished_0.26.0.pk3`
- `unvanquished_0.27.0.pk3`
- `unvanquished_0.28.0.pk3`
- `unvanquished_0.29.0.pk3`
- `unvanquished_0.30.0.pk3`
- `unvanquished_0.31.0.pk3`
- `unvanquished_0.32.0.pk3`
- `unvanquished_0.33.0.pk3`
- `unvanquished_0.34.0.pk3`
- `unvanquished_0.35.0.pk3`
- `unvanquished_0.36.0.pk3`
- `unvanquished_0.37.0.pk3`
- `unvanquished_0.38.0.pk3`
- `unvanquished_0.39.0.pk3`
- `unvanquished_0.40.0.pk3`
- `unvanquished_0.41.0.pk3`
- `unvanquished_0.42.0.pk3`
- `unvanquished_0.43.0.pk3`
- `unvanquished_0.44.0.pk3`
- `unvanquished_0.45.0.pk3`
- `unvanquished_0.46.0.pk3`
- `unvanquished_0.47.0.pk3`
- `unvanquished_0.48.0.pk3`
- `unvanquished_0.49.0.pk3`
- `unvanquished_0.50.0.pk3`

All files that were already in the [https://github.com/Unvanquished/Unvanquished](Unvanquished) repository (mainly in `main/`) directory were not included in this repository from the start. The idea is to delete here all files we are sure they will be never used anymore, and to substitute lossy files with their lossless counterpart. When something is clean, clean repository must be created to store them, and when clean stuff are imported in clean repository, they must be deleted from there. The cleaning job is done when this `README.md` file is the only file left in this repository.

This is an attempt to cleanly reorganize and manage assets from the [Unvanquished](http://unvanquished.net) game.
